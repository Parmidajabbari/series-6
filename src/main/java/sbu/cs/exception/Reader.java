package sbu.cs.exception;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */

    public void readTwitterCommands(List<String> args) throws ApException{
        for (int i = 0; i < args.size(); i++) {
            if(Util.getNotImplementedCommands().contains(args.get(i))) {
                throw new NotImplementedCommand();
            }
        }
        for (int j = 0; j < args.size(); j++) {
            if(!Util.getNotImplementedCommands().contains(args.get(j))) {
                if(!Util.getImplementedCommands().contains(args.get(j)))
                throw new UnknownCommand();
            }
        }


}

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String...args) throws ApException{
        for(int i = 1 ; i < args.length ; i=i+2) {
            try {
                Integer.parseInt(args[i]);
            }
            catch (Exception e){
                throw new NumberFormatException();
            }

        }

    }
}
