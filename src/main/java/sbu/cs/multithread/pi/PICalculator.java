package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) throws InterruptedException{
        PInumber.PInumber = BigDecimal.valueOf(0);
        final ExecutorService execute = Executors.newFixedThreadPool(8);
        for (int i = 0; i <= floatingPoint; i++) {
            BigDecimal ib = new BigDecimal(i);
            execute.submit(new CalculateBailey(ib));
        }
        execute.shutdown();
        execute.awaitTermination(10, TimeUnit.SECONDS);
        String PI = "";
        for(int i = 0 ; i < floatingPoint ; i++) {

        }
        return PInumber.getPInumber().toString().substring(0,floatingPoint+2);
    }
}
