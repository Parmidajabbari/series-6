package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Calculate implements Runnable{
    private BigDecimal n;
    private static final BigDecimal minus1 = new BigDecimal(-1);
    private static final BigDecimal two = new BigDecimal(2);
    private static final BigDecimal one = new BigDecimal(1);
    public static MathContext mathContext = new MathContext(1000,RoundingMode.HALF_UP);
    public BigDecimal pow(BigDecimal a, BigDecimal b) {
        BigDecimal i;
        BigDecimal Power = new BigDecimal(1);
        for(i = BigDecimal.valueOf(0); i.compareTo(b) < 0; i=i.add(one)){
            Power=Power.multiply(a);
        }
        return Power;
    }
    public Calculate(BigDecimal n) {
        this.n = n;
    }
    @Override
    public void run() {
        BigDecimal temp = (n.multiply(two)).add(one);
        BigDecimal bigDecimal = pow(minus1,n).divide(temp,mathContext);
        PInumber.addPInumber(bigDecimal);
    }
}
