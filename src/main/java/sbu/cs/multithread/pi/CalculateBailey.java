package sbu.cs.multithread.pi;


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class CalculateBailey implements Runnable {
    private BigDecimal n;
    private static final BigDecimal minus1 = new BigDecimal(-1);
    private static final BigDecimal one = new BigDecimal(1);
    public static MathContext mathContext = new MathContext(10000, RoundingMode.FLOOR);

    public CalculateBailey(BigDecimal n) {
        this.n = n;
    }

    public BigDecimal pow(BigDecimal a, BigDecimal b) {
        BigDecimal i;
        BigDecimal Power = new BigDecimal(1);
        for (i = BigDecimal.valueOf(0); i.compareTo(b) < 0; i = i.add(one)) {
            Power = Power.multiply(a);
        }
        return Power;
    }

    @Override
    public void run() {
        BigDecimal Rfix = pow(BigDecimal.valueOf(16),n);
        BigDecimal fix = n.multiply(BigDecimal.valueOf(8));
        BigDecimal R1 = BigDecimal.valueOf(4).divide(fix.add(one),mathContext);
        BigDecimal R2 = BigDecimal.valueOf(-2).divide(fix.add(BigDecimal.valueOf(4)),mathContext);
        BigDecimal R3 = minus1.divide(fix.add(BigDecimal.valueOf(5)),mathContext);
        BigDecimal R4 = minus1.divide(fix.add(BigDecimal.valueOf(6)),mathContext);
        BigDecimal bigDecimal =R1.add(R2).add(R3).add(R4);
        PInumber.addPInumber(bigDecimal.divide(Rfix,mathContext));
    }

}