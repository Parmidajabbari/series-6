package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;

public class PInumber {
    public static BigDecimal PInumber = new BigDecimal(0,new MathContext(10000));
    synchronized static public void addPInumber(BigDecimal decimal) {
        PInumber = PInumber.add(decimal);
    }
    public static BigDecimal getPInumber() {
        return PInumber;
    }
}
